<?php
namespace Deployer;

require 'recipe/laravel.php';
require 'vendor/deployer/recipes/recipe/npm.php';

// Project name
set('application', 'back-office-api');

// Project repository
set('repository', 'git@bitbucket.org:ahreji5751/back-office-api.git');

// [Optional] Allocate tty for git clone. Default value is false.
set('git_tty', true);

// Shared files/dirs between deploys 
add('shared_files', []);
add('shared_dirs', []);

set('composer_options', 'install --verbose --prefer-dist --no-progress --no-interaction --optimize-autoloader');

// Writable dirs by web server 
add('writable_dirs', []);

// Hosts
host('my-online-office.ru')
    ->stage('production')
    ->roles('app')
    ->set('deploy_path', '/var/www/html/{{application}}')
    ->user('root')
    ->port(22)
    ->identityFile('~/.ssh/back_rsa');

// Tasks
task('deploy:build', function () {
    run('cd {{release_path}} && npm run prod');
});

task('deploy:upload:env', function () {
    run('cp {{release_path}}/.env.production {{deploy_path}}/shared/.env');
})->desc('Environment setup');

task('deploy:run:tests', function() {
    run('cd {{release_path}} && vendor/bin/phpunit');
});

task('deploy:run:queue:worker', function() {
    run('cd {{release_path}} && pm2 start laravel-queue-worker.yml');
});

after('deploy:update_code', 'npm:install');

after('npm:install', 'deploy:build');

after('deploy:shared', 'deploy:upload:env');

after('deploy:vendors', 'deploy:run:queue:worker');

after('deploy:run:queue:worker', 'deploy:run:tests');

// [Optional] if deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');

// Migrate database before symlink new release.

before('deploy:symlink', 'artisan:migrate');

