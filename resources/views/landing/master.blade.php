<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <meta charset="utf-8">
  <meta charset="utf-8" name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="Бухгалтерский аутсорсинг - онлайн офис для бизнеса">
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>Мой онлайн офис</title>

  <link rel="stylesheet" type="text/css" href="{{ mix('/css/app.css') }}">
</head>
<body>
  <main>
    <div id="app" sticky-container="{ zIndex: 1000 }">
      @include('landing.blocks.menu')

      @yield('content')

      @include('landing.blocks.footer')

      <scroll-to-top-button></scroll-to-top-button>
    </div>
  </main>

  <script src="{{ mix('/js/app.js') }}"></script>
  <script src="{{ mix('/js/manifest.js') }}"></script>
  <script src="{{ mix('/js/vendor.js') }}"></script>

  <!-- Yandex.Metrika counter -->
  <script type="text/javascript" >
    (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
      m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
    (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

    ym(57877999, "init", {
      clickmap:true,
      trackLinks:true,
      accurateTrackBounce:true,
      webvisor:true
    });
  </script>
  <noscript><div><img src="https://mc.yandex.ru/watch/57877999" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
  <!-- /Yandex.Metrika counter -->

</body>
</html>
