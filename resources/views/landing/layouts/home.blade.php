@extends('landing.master')

@section('content')

  @include('landing.blocks.promo')

  @include('landing.blocks.online-accounting')

  @include('landing.blocks.business-registration')

  @include('landing.blocks.hr-document-flow')

  @include('landing.blocks.ved')

{{--  @include('landing.blocks.business-from-scratch')--}}

{{--  @include('landing.blocks.application')--}}

  <prices></prices>

  @include('landing.blocks.custom-tasks')

@endsection

