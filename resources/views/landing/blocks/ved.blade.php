<div class="ved-container">
  <div id="ved"  class="container">
    <div class="row">
      <div class="col-xl-6 col-lg-12 col-md-12 d-flex flex-column align-items-xl-start align-items-lg-start align-items-center">
        <div class="ved-container__title">ВЭД / <br class="d-xl-block d-lg-block d-none">возврат экспортного НДС</div>
        <div class="ved-container__separator"></div>
        <div class="ved-container__text">
          Если перед вашей компанией стоит задача грамотно
          провести внешне-торговую операцию или возместить НДС
          - лучший выбор доверить это профессионалам.
          Положительный результат в этом процессе зависит
          от понимания построения облагаемой базы по НДС
          и опыта специалиста в области налогового права
        </div>
        <div class="ved-container__action">
          <button class="bo-button-primary" v-scroll-to="'#custom-tasks-form'">Оставить заявку</button>
        </div>
      </div>
      <div class="col-xl-6 col-lg-12 col-md-12">
        <ved-tabs />
      </div>
    </div>
  </div>
</div>