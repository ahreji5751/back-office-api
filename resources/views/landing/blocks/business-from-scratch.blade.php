<div id="business-from-scratch" class="business-from-scratch">
  <div class="business-from-scratch__grow-background"></div>
  <div class="container">
    <div class="row flex-column align-items-center">
      <div class="business-from-scratch__title">
        Бизнес с нуля
      </div>
      <div class="business-from-scratch__separator"></div>
    </div>
    <div class="row justify-content-center">
      <div class="col-xl-3 col-lg-3 col-md-3 col-sm-8 col-xs-8 col-8 business-from-scratch__info-container">
        <div class="business-from-scratch__svg-container">
          @svg('cashbox', 'business-from-scratch__svg')
        </div>
        <div class="business-from-scratch__description">
          Установка кассы и внедрение системы учёта
        </div>
      </div>
      <div class="col-xl-3 col-lg-3 col-md-3 col-sm-8 col-xs-8 col-8 business-from-scratch__info-container">
        <div class="business-from-scratch__svg-container">
          @svg('cart', 'business-from-scratch__svg')
        </div>
        <div class="business-from-scratch__description">
          Подготовка пакета документов для интернет-магазина
        </div>
      </div>
      <div class="col-xl-3 col-lg-3 col-md-3 col-sm-8 col-xs-8 col-8 business-from-scratch__info-container">
        <div class="business-from-scratch__svg-container">
          @svg('store', 'business-from-scratch__svg')
        </div>
        <div class="business-from-scratch__description">
          Помощь в подборе локации офиса или торгового места
        </div>
      </div>
      <div class="col-xl-3 col-lg-3 col-md-3 col-sm-8 col-xs-8 col-8 business-from-scratch__info-container">
        <div class="business-from-scratch__svg-container">
          @svg('application', 'business-from-scratch__svg')
        </div>
        <div class="business-from-scratch__description">
          Разработка приложения под условия вашего бизнеса
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-xl-4 col-lg-4 col-md-4 d-xl-flex d-lg-flex d-none offset-4 justify-content-center">
        <button class="bo-button-primary" v-scroll-to="'#custom-tasks-form'">Оставить заявку</button>
      </div>
    </div>
  </div>
</div>