<div id="application" class="application">
  <div class="container">
    <div class="row flex-column align-items-center">
      <div class="application__title">
        Приложение
      </div>
      <div class="application__separator"></div>
    </div>
    <div class="row">
      <div class="col-xl-5 col-lg-12 col-md-12 mb-lg-5 mb-md-5">
        <div class="application__text application__text--desktop">
          <div class="mb-4">
            Онлайн офис всегда под рукой и  работает на всех
            устройствах от компьютера до телефона.
          </div>
          <div class="mb-4">
            Приложение позволяет получать необходимые
            для вашего бизнеса услуги, общаться со специалистами
            и ставить им задачи, присылать  документы в онлайн режиме.
          </div>
          <div class="mb-4">
            Вам больше не потребуется совершать лишние звонки,
            посещать офис или искать нужного специалиста
            в бухгалтерской, юридической, налоговой и кадровой сфере -
            все они находятся на связи для решения ваших вопросов
          </div>
        </div>
      </div>
      <div class="col-xl-7 col-lg-12 col-md-12 d-flex justify-content-center">
        @svg('application_desktop', 'application__desktop-svg')
      </div>
    </div>


    <div class="row mt-xl-5 mt-lg-5 mt-md-5">
      <div class="col-xl-5 col-lg-12 col-md-12 mb-lg-5 mb-md-5 d-flex justify-content-center order-xl-1 order-2">
        @svg('application_tablet', 'application__tablet-svg')
      </div>
      <div class="offset-xl-1 col-xl-6 col-lg-12 col-md-12 mb-xl-5 mb-lg-5 d-flex align-items-center justify-content-center order-xl-2 order-1">
        <div class="application__points">
          <div class="application__points-text">Преимущества, которые получают наши клиенты:</div>
          <div class="application__point">
            <div class="application__point-svg">@svg('checked')</div>
            <div class="application__point-text">при наличии гаджета, бизнес можно вести из любой точки мира</div>
          </div>
          <div class="application__point">
            <div class="application__point-svg">@svg('checked')</div>
            <div class="application__point-text">
              вы не переплачиваете зарплату, налоги штатным сотрудникам,
              а выбираете необходимый вашему бизнему спектр операций
            </div>
          </div>
          <div class="application__point">
            <div class="application__point-svg">@svg('checked')</div>
            <div class="application__point-text">
              на вас работает команда профессиональных специалистов,
              находящихся на связи без больничных и отпусков, ведь в команде
              действует принцип полной взаимозаменяемости
            </div>
          </div>
          <div class="application__point">
            <div class="application__point-svg">@svg('checked')</div>
            <div class="application__point-text">
              гибкая система позволяет в любой момент  дополнить
              выбранный вами перечень услуг
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row mt-5">
      <div class="col-xl-6 col-lg-12 col-md-12 mb-xl-5 mb-lg-5 d-flex flex-column align-items-start justify-content-center">
        <div class="application__points mb-xl-5 mb-lg-5">
          <div class="application__points-text">Как работает приложение:</div>
          <div class="application__point">
            <div class="application__point-digit">1</div>
            <div class="application__point-text">зарегистрируйтесь в личном кабинете</div>
          </div>
          <div class="application__point">
            <div class="application__point-digit">2</div>
            <div class="application__point-text">выберите необходимую вам услугу</div>
          </div>
          <div class="application__point">
            <div class="application__point-digit">3</div>
            <div class="application__point-text">совершите оплату</div>
          </div>
          <div class="application__point">
            <div class="application__point-digit">4</div>
            <div class="application__point-text">
              ставьте задачи специалистам, задавайте вопросы,
              загружайте документы в удобном для вас формате
            </div>
          </div>
          <div class="application__point">
            <div class="application__point-digit">5</div>
            <div class="application__point-text">
              следите за выполнением дел в приложении
            </div>
          </div>
        </div>
        <button class="bo-button-primary d-xl-block d-lg-block d-none">Начать работу</button>
      </div>
      <div class="col-xl-6 col-lg-12 col-md-12 mb-lg-5 mb-md-5 d-flex justify-content-center">
        @svg('application_phone', 'application__mobile-svg')
      </div>
      <div class="col-md-12 col-sm-12 col-xs-12 col-12 d-xl-none d-lg-none d-flex justify-content-center">
        <button class="bo-button-primary">Начать работу</button>
      </div>
    </div>
  </div>
</div>