<div id="custom-tasks" class="custom-tasks">
  <div class="container">
    <div class="row flex-column align-items-center">
      <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 col-12">
        <div class="custom-tasks__title">
          Нестандартные задачи
        </div>
      </div>
      <div class="custom-tasks__separator"></div>
      <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 col-12">
        <div class="custom-tasks__text">
          Возможно, ваш бизнес уникален и требует специализированного подхода в решении
          нестандартных задач.
          Оставьте заявку, мы подберём из числа профессионалов
          команду, которая подготовит и реализует персональный бизнес-кейс.
          Специалисты: оценка, банкротство, автоматизация бизнеса (программа 1С,
          ККМ, маркировка товаров), Ваш юрист, подготовка пакета документов для интернет-магазина,
          интернет-маркетинг и другие
        </div>
      </div>
    </div>
    <div class="row justify-content-center">
      <button class="bo-button-primary" v-scroll-to="'#custom-tasks-form'">Оставить заявку</button>
    </div>
    <div class="row justify-content-center">
      <custom-tasks-feedback-form />
    </div>
  </div>
</div>