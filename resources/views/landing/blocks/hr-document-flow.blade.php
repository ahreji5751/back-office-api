<div id="hr-document-flow" class="hr-document-flow">
  <div class="hr-document-flow__arrows-background"></div>
  <div class="container">
    <div class="row flex-column align-items-center">
      <div class="hr-document-flow__title">
        HR-документооборот
      </div>
      <div class="hr-document-flow__separator"></div>
      <div class="hr-document-flow__description">
        Поручите ведение кадровых дел специалисту
      </div>
    </div>
    <div class="row">
      <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-xs-12 col-12">
        <div class="hr-document-flow__img-container">
          <div class="hr-document-flow__img hr-document-flow__img1"></div>
          <div class="hr-document-flow__img-description">
            Составление кадровых документов в рамках трудового законодательства
          </div>
        </div>
      </div>
      <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-xs-12 col-12">
        <div class="hr-document-flow__img-container">
          <div class="hr-document-flow__img hr-document-flow__img2"></div>
          <div class="hr-document-flow__img-description">
            Аутсорсинг кадрового делопроизводства
          </div>
        </div>
      </div>
      <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-xs-12 col-12">
        <div class="hr-document-flow__img-container">
          <div class="hr-document-flow__img hr-document-flow__img3"></div>
          <div class="hr-document-flow__img-description">
            Помощь в оформлении на работу иностранного сотрудника
          </div>
        </div>
      </div>
      <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-xs-12 col-12">
        <div class="hr-document-flow__img-container">
          <div class="hr-document-flow__img hr-document-flow__img4"></div>
          <div class="hr-document-flow__img-description">
            Ведение учёта заработной платы, декретные выплаты
          </div>
        </div>
      </div>
    </div>
  </div>
</div>