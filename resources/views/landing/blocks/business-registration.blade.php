<div id="business-registration" class="business-registration">
  <div class="business-registration__gears-background"></div>
  <div class="container">
    <div class="row justify-content-center">
      <div class="business-registration__title text-center">
        Регистрация  / <br> ликвидация бизнеса
      </div>
    </div>
    <div class="row">
      <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-xs-12 col-12 d-flex justify-content-center">
        <div class="business-registration-block">
          <span class="business-registration-block__title">Чем мы сможем помочь</span>
          <div class="business-registration-block__point">
            <div class="business-registration-block__point-digit">1</div>
            <div class="business-registration-block__point-text">
              подготовим полный пакет документов для регистрации ИП и юридического лица
            </div>
          </div>
          <div class="business-registration-block__point">
            <div class="business-registration-block__point-digit">2</div>
            <div class="business-registration-block__point-text">
              внесем изменения в регистрационные данные
            </div>
          </div>
          <div class="business-registration-block__point">
            <div class="business-registration-block__point-digit">3</div>
            <div class="business-registration-block__point-text">
              осуществим процедуру закрытия бизнеса
            </div>
          </div>
          {{--<div class="business-registration-block__description">
            За вами останется выбор - сделать это самостоятельно*
            или воспользоваться услугами наших специалистов
            и поручить им оформление документов «под ключ»
          </div>
          <div class="business-registration-block__note">
            *Доступно только для услуг регистрации
            ООО/ИП и внесение изменений в
            регистрационные данные
          </div>--}}
          <div class="business-registration-block__button">
            <button class="bo-button-secondary" v-scroll-to="'#prices'">Узнать цену</button>
          </div>
        </div>
      </div>
      <div class="col-md-12 col-sm-12 col-xs-12 col-12 d-xl-none d-lg-none d-flex justify-content-center mt-4 mb-4">
        <i class="fas fa-chevron-down main-color"></i>
      </div>
      <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-xs-12 col-12 d-flex justify-content-center">
        <div class="business-registration-block">
          <span class="business-registration-block__title">Как это работает</span>
          <div class="business-registration-block__point">
            <div class="business-registration-block__point-digit">1</div>
            <div class="business-registration-block__point-text">
              отправьте нам информацию о вашем бизнесе
            </div>
          </div>
          <div class="business-registration-block__point">
            <div class="business-registration-block__point-digit">2</div>
            <div class="business-registration-block__point-text">
              наш специалист подготовит документы, заверит их у нотариуса и передаст в ФНС для регистрации
            </div>
          </div>
          <div class="business-registration-block__point">
            <div class="business-registration-block__point-digit">3</div>
            <div class="business-registration-block__point-text">
              получите готовый пакет документов  удобным для вас способом
            </div>
          </div>
          {{--<div class="business-registration-block__description">
            Вы не переплачиваете юристам, нотариусам
            и сможете обойтись без визита в госорганы
          </div>--}}
          <div class="business-registration-block__button">
            <button class="bo-button-primary" v-scroll-to="'#custom-tasks-form'">Отправить заявку</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>