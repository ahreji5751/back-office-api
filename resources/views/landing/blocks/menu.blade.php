<div class="contacts">
  <div class="container">
    <div class="row align-items-center justify-content-end h-100">
      <div class="contacts__item contacts__item--phone">
        @svg('phone', 'contacts__logo')
        <a href="tel:+79107480234" class="contacts__text">+7 (910) 748-02-34</a>
      </div>
      <div class="contacts__item contacts__item--mail">
        @svg('mail', 'contacts__logo')
        <a href="mailto:hello@my-online-office.ru" class="contacts__text">hello@my-online-office.ru</a>
      </div>
    </div>
  </div>
</div>
<global-menu></global-menu>