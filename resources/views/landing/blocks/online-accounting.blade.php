<div id="online-accounting" class="online-accounting">
  <div class="container">
    <div class="row flex-column align-items-center">
      <div class="online-accounting__title">
        Онлайн бухгалтерия
      </div>
      <div class="online-accounting__separator"></div>
    </div>
    <div class="row">
      <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-xs-12 col-12 p-0 d-flex flex-column justify-content-between">
        <div class="info-block justify-content-xl-end justify-content-lg-end justify-content-md-center justify-content-sm-center justify-content-center">
          <div class="info-block__text align-content-end order-xl-0 order-lg-0 order-1">
            <div class="info-block__title text-xl-right text-lg-right text-left">Бухгалтерия</div>
            <div class="info-block__description text-xl-right text-lg-right text-left">
              Окажем бухгалтерские услуги, подготовим документы и отчёты
            </div>
          </div>
          <div class="info-block__image-container order-xl-1 order-lg-1 order-0">
            @svg('calc', 'info-block__calc')
          </div>
        </div>
        <div class="info-block justify-content-xl-end justify-content-lg-end justify-content-md-center justify-content-sm-center justify-content-center">
          <div class="info-block__text align-content-end order-xl-0 order-lg-0 order-1">
            <div class="info-block__title text-xl-right text-lg-right text-left">Налоги</div>
            <div class="info-block__description text-xl-right text-lg-right text-left">
              Сформируем и вовремя отправим налоговые декларации, напомним о сроках оплаты налогов
            </div>
          </div>
          <div class="info-block__image-container order-xl-1 order-lg-1 order-0">
            @svg('notifications', 'info-block__notifications')
          </div>
        </div>
        <div class="info-block justify-content-xl-end justify-content-lg-end justify-content-md-center justify-content-sm-center justify-content-center">
          <div class="info-block__text align-content-end order-xl-0 order-lg-0 order-1">
            <div class="info-block__title text-xl-right text-lg-right text-left">Выбор системы налогообложения</div>
            <div class="info-block__description text-xl-right text-lg-right text-left">
              Вычислим оптимальную налоговую нагрузку
            </div>
          </div>
          <div class="info-block__image-container order-xl-1 order-lg-1 order-0">
            @svg('tax_tablet', 'info-block__tax-tablet')
          </div>
        </div>
        <div class="info-block justify-content-xl-end justify-content-lg-end justify-content-md-center justify-content-sm-center justify-content-center">
          <div class="info-block__text align-content-end order-xl-0 order-lg-0 order-1">
            <div class="info-block__title text-xl-right text-lg-right text-left">Общение с налоговыми органами</div>
            <div class="info-block__description text-xl-right text-lg-right text-left">
              Поможем решить накопившиеся вопросы и споры с налоговыми органами
            </div>
          </div>
          <div class="info-block__image-container order-xl-1 order-lg-1 order-0">
            @svg('balance', 'info-block__balance')
          </div>
        </div>
      </div>
      <div class="col-xl-4 col-lg-4 col-md-12 col-xs-12 col-12 d-xl-flex d-lg-flex d-none justify-content-center">
        <div class="online-accounting__phone-picture-container text-center">
          @svg('big_phone', 'online-accounting__phone-picture')
          <div class="online-accounting__phone-actions">
            <button class="bo-button-primary bo-button-primary--wide mb-5" v-scroll-to="'#custom-tasks-form'">
              Оставить заявку
            </button>
            <button class="bo-button-primary bo-button-primary--wide" v-scroll-to="'#prices'">Узнать цену</button>
          </div>
        </div>
      </div>
      <div class="col-xl-4 col-lg-4 col-md-12 col-xs-12 col-12 p-0 d-flex flex-column justify-content-between images-right-column">
        <div class="info-block justify-content-xl-start justify-content-lg-start justify-content-md-center justify-content-sm-center justify-content-center">
          <div class="info-block__image-container">
            @svg('transactions', 'info-block__transactions')
          </div>
          <div class="info-block__text align-content-start">
            <div class="info-block__title">Проведение платежей</div>
            <div class="info-block__description text-left">
              Подготовим платёжные документы для проведения расчётов
            </div>
          </div>
        </div>
        <div class="info-block justify-content-xl-start justify-content-lg-start justify-content-md-center justify-content-sm-center justify-content-center">
          <div class="info-block__image-container">
            @svg('documents', 'info-block__documents')
          </div>
          <div class="info-block__text align-content-start">
            <div class="info-block__title text-left">Оформление претензий по задолженностям</div>
            <div class="info-block__description text-left">
              Произведём сверку расчётов и оформим претензии по имеющимся задолженностям
            </div>
          </div>
        </div>
        <div class="info-block justify-content-xl-start justify-content-lg-start justify-content-md-center justify-content-sm-center justify-content-center">
          <div class="info-block__image-container">
            @svg('unblock', 'info-block__unblock')
          </div>
          <div class="info-block__text align-content-start">
            <div class="info-block__title text-left">Разблокировка счёта</div>
            <div class="info-block__description text-left">
              Разберёмся по какой причине заблокировали счёт и восстановим его работу
            </div>
          </div>
        </div>
        <div class="info-block m-0 justify-content-xl-start justify-content-lg-start justify-content-md-center justify-content-sm-center justify-content-center">
          <div class="info-block__image-container">
            @svg('bank', 'info-block__bank')
          </div>
          <div class="info-block__text align-content-start">
            <div class="info-block__title text-left">Подбор банка обслуживания</div>
            <div class="info-block__description text-left">
              Подберём банк с учётом особенностей вашего бизнеса
            </div>
          </div>
        </div>
      </div>
      <div class="d-xl-none d-lg-none d-flex col-md-12 col-sm-12 col-xs-12 col-12 mt-5">
        <div class="col-md-6 col-sm-6 col-xs-6 col-6 p-0 d-flex justify-content-end">
          <div class="bo-button-secondary" v-scroll-to="'#prices'">Узнать цену</div>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-6 col-6 p-0 d-flex justify-content-start">
          <div class="bo-button-primary" v-scroll-to="'#custom-tasks-form'">Оставить заявку</div>
        </div>
      </div>
    </div>
  </div>
</div>

