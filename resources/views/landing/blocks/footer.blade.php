<div class="footer">
  <div class="container">
    <div class="footer__description">
      ООО «Мой онлайн офис» 2020.
      <br>
      ИНН:  5752080700   ОГРН: 1195749001546
      <br>
      Телефон: <a href="tel:+79107480234">+7 (910) 748-02-34</a>
      <br>
      <span class="d-flex align-items-center">
        E-mail: &nbsp;<a href="mailto:hello@my-online-office.ru">hello@my-online-office.ru</a>
        <a href="https://t.me/my_online_office" target="_blank" rel="noreferrer" class="ml-3">
          <img class="mobile-menu__telegram-image" src="{{ URL::to('/') }}/images/telegram.png" alt="no_image" />
        </a>
      </span>
    </div>
  </div>

  {{--<div class="container">
    <div class="row">
      <div class="col-xl-2 col-lg-2 col-md-12 col-sm-12 col-xs-12 col-12 d-flex justify-content-xl-center justify-content-lg-center justify-content-start mb-1">
        <div class="footer__text">
          <a href="">Кто мы</a>
        </div>
      </div>
      <div class="col-xl-2 col-lg-2 col-md-12 col-sm-12 col-xs-12 col-12 d-flex justify-content-xl-center justify-content-lg-center justify-content-start mb-5">
        <div class="footer__text">
          <a href="">Документы</a>
        </div>
      </div>
      <div class="col-xl-2 col-lg-2 col-md-12 col-sm-12 col-xs-12 col-12 d-flex justify-content-xl-center justify-content-lg-center justify-content-start mb-5">
        <div class="footer__text">
          <a href="">Информация</a>
        </div>
      </div>
      <div class="col-xl-2 col-lg-2 col-md-12 col-sm-12 col-xs-12 col-12 d-flex justify-content-xl-center justify-content-lg-center justify-content-start mb-1">
        <div class="footer__text">
          <a href="">Вакансии</a>
        </div>
      </div>
      <div class="col-xl-2 col-lg-2 col-md-12 col-sm-12 col-xs-12 col-12 d-flex justify-content-xl-center justify-content-lg-center justify-content-start mb-5">
        <div class="footer__text">
          <a href="">Контакты и реквизиты</a>
        </div>
      </div>
      <div class="col-xl-2 col-lg-2 col-md-12 col-sm-12 col-xs-12 col-12">
        <div class="footer__text mb-3">
          <a href="">Телефон</a>
          <div class="footer__description">
            +7 (495) 669-12-75
          </div>
        </div>
        <div class="footer__text mb-4">
          E-mail
          <div class="footer__description">
            m.onlineoffice@gmail.com
          </div>
        </div>
        <div class="footer__text">
          Офис
          <div class="footer__description">
            г. Орел, ул. 2-Посадская, д. 4, пом. 100
          </div>
        </div>
      </div>
    </div>
  </div>--}}
</div>