import Vue from 'vue';
import VueScrollTo from 'vue-scrollto';
import Sticky from 'vue-sticky-directive';
import vSelect from 'vue-select';

import PromoAnimation from './landing/components/PromoAnimation';
import VedTabs from './landing/components/VedTabs';
import CustomTasksFeedbackForm from './landing/components/CustomTasksFeedbackForm';
import GlobalMenu from './landing/components/GlobalMenu';
import ScrollToTopButton from './landing/components/ScrollToTopButton';
import Prices from './landing/components/Prices';

Vue.use(VueScrollTo, {
  container: 'body',
  duration: 700,
  easing: 'ease-in-out',
  offset: -90,
  force: true,
  cancelable: true,
  onStart: false,
  onDone: false,
  onCancel: false,
  x: false,
  y: true
});
Vue.use(Sticky);
Vue.component('v-select', vSelect);

new Vue({
  el: '#app',
  components: {
    'promo-animation': PromoAnimation,
    'ved-tabs': VedTabs,
    'custom-tasks-feedback-form': CustomTasksFeedbackForm,
    'global-menu': GlobalMenu,
    'scroll-to-top-button': ScrollToTopButton,
    'prices': Prices
  }
});
