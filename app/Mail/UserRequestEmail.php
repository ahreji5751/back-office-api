<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserRequestEmail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**@var string*/
    public $name;

    /**@var string*/
    public $contact;

    /**@var string*/
    public $myMessage;

    /**
     * Create a new message instance.
     *
     * @param string $name
     * @param string $contact
     * @param string $message
     * @return void
     */
    public function __construct($name, $contact, $message)
    {
        $this->name = $name;
        $this->contact = $contact;
        $this->myMessage = $message;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('myonlineoffice@mail.com', 'Приложение "Мой онлайн офис"')
                    ->subject('Заявка от пользователя')
                    ->view('mail.user-request');
    }
}
