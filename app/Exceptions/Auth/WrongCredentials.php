<?php

namespace App\Exceptions\Auth;

use Exception;
use Illuminate\Contracts\Support\Responsable;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class WrongCredentials extends Exception implements Responsable
{
    /**
     * Create an HTTP response that represents the object.
     *
     * @param  Request $request
     * @return Response
     */
    public function toResponse($request)
    {
        return response()->json([
            'success' => false,
            'text' => 'Wrong credentials or user phone not verified'
        ], 401);
    }
}
