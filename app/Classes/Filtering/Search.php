<?php

namespace App\Classes\Filtering;

use Illuminate\Http\Request;

class Search
{
    public static function apply(Request $request, $query)
    {
        $query = static::applyDecoratorsFromRequest($request, $query);

        return static::getResults($query);
    }

    private static function applyDecoratorsFromRequest(Request $request, $query)
    {
        foreach ($request->all() as $filterName => $value) {

            $decorator = static::createFilterDecorator($filterName);

            if (static::isValidDecorator($decorator)) {
                $query = $decorator::apply($query, $value);
            }

        }
        return $query;
    }

    private static function createFilterDecorator($name)
    {
        return __NAMESPACE__ . '\\Filters\\' . str_replace(' ', '', ucwords(str_replace('_', ' ', $name)));
    }

    private static function isValidDecorator($decorator)
    {
        return class_exists($decorator);
    }

    private static function getResults($query)
    {
        return $query->get();
    }
}