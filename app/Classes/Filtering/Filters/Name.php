<?php

namespace App\Classes\Filtering\Filters;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOneOrMany;

class Name implements Filter
{
    /**
     * Apply a given search value to the builder instance.
     *
     * @param Builder|HasMany|HasOneOrMany $query
     * @param mixed $value
     * @return Builder $builder
     */
    public static function apply($query, $value)
    {
        return $query->where('name', 'like', "%$value%");
    }
}