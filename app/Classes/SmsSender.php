<?php

namespace App\Classes;

use App;
use GuzzleHttp\Client;
use App\Exceptions\SmsSenderException;
use GuzzleHttp\Exception\GuzzleException;

class SmsSender
{
    /**
     * @var Client $client
     */
    private $client;

    /**
     * Create new SmsSender instance
     *
     * @return void
     */
    public function __construct()
    {
        $this->client = new Client();
    }

    /**
     * Send code to the user via sms
     *
     * @param string $code
     * @param string $phone
     * @throws SmsSenderException
     * @return void
     */
    public function sendCode($code, $phone)
    {
        $message = preg_replace('/{code}/', $code, config('sms-sender.message'));
        $this->sendSms($phone, $message);
    }

    /**
     * Send sms to the user
     *
     * @param string $phone
     * @param string $message
     * @throws SmsSenderException
     * @return void
     */
    private function sendSms($phone, $message)
    {
        if (app()->environment('local') || config('sms-sender.disable_sms')) return;

        try {
            $response = $this->client->request('POST', config('sms-sender.endpoint'), [
                'form_params' => [
                    'login' => config('sms-sender.login'),
                    'psw' => config('sms-sender.password'),
                    'phones' => $phone,
                    'mes' => $message
                ]
            ]);
        } catch(GuzzleException $e) {
            return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ], 400);
        }

        $responseString = $response->getBody()->getContents();
        if (preg_match('/ERROR/', $responseString)) {
            throw new SmsSenderException($responseString);
        }
    }
}