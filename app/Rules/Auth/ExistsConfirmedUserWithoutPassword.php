<?php

namespace App\Rules\Auth;

use App\Models\User;
use Illuminate\Contracts\Validation\Rule;

class ExistsConfirmedUserWithoutPassword implements Rule
{
    /**
     * Error message
     *
     * @var string
     */
    private $errorMessage;

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $user = User::where($attribute, $value)->first();

        if (!$user) {
            $this->errorMessage = 'User with that phone doesn\'t exists';
            return false;
        }

        if (!$user->phone_verified || $user->password) {
            $this->errorMessage = 'User already has the password';
            return false;
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->errorMessage;
    }
}
