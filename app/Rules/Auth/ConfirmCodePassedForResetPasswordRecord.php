<?php

namespace App\Rules\Auth;

use App\Models\User;
use Illuminate\Contracts\Validation\Rule;

class ConfirmCodePassedForResetPasswordRecord implements Rule
{
    /**
     * Error message
     *
     * @var string
     */
    private $errorMessage;

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $user = User::where($attribute, $value)->first();

        if (!$user) {
            $this->errorMessage = 'User with that phone doesn\'t exists';
            return false;
        }

        if (!$resetPasswordRow = $user->resetPassword()->first()) {
            $this->errorMessage = 'Password restore request doesn\'t found for that phone';
            return false;
        }

        $this->errorMessage = 'Invalid or expired validation code';

        return $resetPasswordRow->confirmCodePassed(request()->code);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->errorMessage;
    }
}
