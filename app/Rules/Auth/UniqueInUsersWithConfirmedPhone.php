<?php

namespace App\Rules\Auth;

use Illuminate\Contracts\Validation\Rule;
use App\Models\User;

class UniqueInUsersWithConfirmedPhone implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $user = User::where($attribute, $value)->first();

        if (!$user) {
            return true;
        }

        if (!$user->phone_verified) {
            return true;
        }

        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'User with this :attribute already exist';
    }
}
