<?php

namespace App\Rules\Auth;

use Illuminate\Contracts\Validation\Rule;
use App\Models\User;

class ResetPasswordRecordExistsForThatUser implements Rule
{
    /**
     * User phone
     *
     * @var string
     */
    private $phone;

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $user = User::where($attribute, $value)->first();

        return boolval($user->resetPassword()->first());
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Password restore request doesn\'t found for that phone';
    }
}
