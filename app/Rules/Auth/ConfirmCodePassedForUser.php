<?php

namespace App\Rules\Auth;

use Illuminate\Contracts\Validation\Rule;
use App\Models\User;

class ConfirmCodePassedForUser implements Rule
{
    /**
     * Phone param from request
     *
     * @var string
     */
    private $phone;

    /**
     * Create a new rule instance.
     *
     * @param string $phone
     * @return void
     */
    public function __construct($phone)
    {
        $this->phone = $phone;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $user = User::where('phone', $this->phone)->first();

        if (!$user) {
            return true;
        }

        if ($user->confirm_code && intval($user->confirm_code) === intval($value)) {
            return true;
        }

        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Invalid or expired validation code';
    }
}
