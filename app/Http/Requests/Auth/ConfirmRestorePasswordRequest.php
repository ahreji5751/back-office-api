<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Rules\Auth\ResetPasswordRecordExistsForThatUser;
use App\Rules\Auth\ConfirmCodePassedForResetPasswordRecord;

class ConfirmRestorePasswordRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'phone' => [
                'bail',
                'required',
                'string',
                'size:12',
                new ConfirmCodePassedForResetPasswordRecord
            ],
            'code' => 'required|digits:6'
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'exists' => 'The user with that :attribute doesn\'t exists'
        ];
    }
}
