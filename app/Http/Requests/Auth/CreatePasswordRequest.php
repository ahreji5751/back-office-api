<?php

namespace App\Http\Requests\Auth;

use App\Http\Requests\JsonRequest;
use App\Rules\Auth\ExistsConfirmedUserWithoutPassword;

class CreatePasswordRequest extends JsonRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'phone' => [
                'bail',
                'required',
                'string',
                'size:12',
                new ExistsConfirmedUserWithoutPassword
            ],
            'password' => 'required|confirmed|min:6|max:64'
        ];
    }
}
