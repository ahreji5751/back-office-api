<?php

namespace App\Http\Requests\Auth;

use App\Http\Requests\JsonRequest;

class RestorePasswordRequest extends JsonRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'phone' => 'required|string|exists:users'
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'exists' => 'The user with that :attribute doesn\'t exists'
        ];
    }
}
