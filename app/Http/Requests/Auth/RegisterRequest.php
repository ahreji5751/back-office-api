<?php

namespace App\Http\Requests\Auth;

use App\Http\Requests\JsonRequest;
use App\Rules\Auth\UniqueInUsersWithConfirmedPhone;

class RegisterRequest extends JsonRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string',
            'email' => ['required', 'email', new UniqueInUsersWithConfirmedPhone],
            'phone' => ['required', 'string', 'size:12', new UniqueInUsersWithConfirmedPhone]
        ];
    }
}
