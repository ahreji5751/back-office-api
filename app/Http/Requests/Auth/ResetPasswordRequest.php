<?php

namespace App\Http\Requests\Auth;

use App\Http\Requests\JsonRequest;

class ResetPasswordRequest extends JsonRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'hash' => 'required|string|exists:reset_passwords,hash,deleted_at,NULL',
            'password' => 'required|confirmed|min:6|max:255'
        ];
    }
}
