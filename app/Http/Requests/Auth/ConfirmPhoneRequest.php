<?php

namespace App\Http\Requests\Auth;

use App\Http\Requests\JsonRequest;
use App\Rules\Auth\ConfirmCodePassedForUser;

class ConfirmPhoneRequest extends JsonRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'phone' => 'required|string|size:12|exists:users',
            'code' => ['required', 'digits:6', new ConfirmCodePassedForUser(request()->phone)]
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'exists' => 'The user with that :attribute doesn\'t exists'
        ];
    }
}
