<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

abstract class JsonRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
    * The data to be validated should be processed as JSON.
    * @return mixed
    */
    protected function validationData()
    {
        return $this->json()->all();
    }
}