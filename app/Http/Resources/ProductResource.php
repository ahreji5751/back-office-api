<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Request;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'units' => $this->units,
            'count' => $this->count,
            'price' => $this->price,
            'nds_rate' => $this->nds_rate,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at
        ];
    }
}
