<?php

namespace App\Http\Controllers;

use App\Classes\Filtering\Search;
use App\Http\Resources\ContractorResource;
use Illuminate\Http\Request;

class ContractorController extends Controller
{
    public function index(Request $request)
    {
        return ContractorResource::collection(Search::apply($request, auth()->user()->contractors()));
    }
}
