<?php

namespace App\Http\Controllers;

use App\Mail\UserRequestEmail;
use Illuminate\Support\Facades\Mail;
use App\Http\Requests\SendUserRequestEmailRequest;
use App\Http\Responses\SuccessResponse;
use Illuminate\Http\JsonResponse;

class UserRequestController extends Controller
{
    /**
     * Handle user request email.
     *
     * @param SendUserRequestEmailRequest $request
     * @return JsonResponse
     */
    public function mail(SendUserRequestEmailRequest $request)
    {
        $validated = $request->validated();
        Mail::to(config('mail.receiver'))
            ->send(new UserRequestEmail(
                $validated['name'],
                $validated['contact'],
                $validated['message']
            ));
        return SuccessResponse::show();
    }
}
