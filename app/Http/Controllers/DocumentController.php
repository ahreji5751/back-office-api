<?php

namespace App\Http\Controllers;

use App\Classes\Filtering\Search;
use App\Http\Resources\DocumentResource;
use App\Models\Document;
use Illuminate\Http\Request;

class DocumentController extends Controller
{
    public function index(Request $request)
    {
        return DocumentResource::collection(Search::apply($request, auth()->user()->documents()->forTheLastYear()));
    }
}
