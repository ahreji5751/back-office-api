<?php

namespace App\Http\Controllers;

use App\Classes\SmsSender;
use App\Http\Requests\ConfirmRestorePasswordRequest;
use App\Http\Responses\SuccessResponse;
use App\Http\Requests\Auth\RestorePasswordRequest;
use App\Http\Requests\Auth\ResetPasswordRequest;
use App\Models\ResetPassword;
use App\Exceptions\SmsSenderException;
use Illuminate\Http\JsonResponse;

class ForgotPasswordController extends Controller
{
    /**
     * User model instance
     *
     * @var ResetPassword
     */
    private $resetPassword;

    /**
     *  Constructor for LoginController.
     *
     * @param ResetPassword $resetPassword
     * @return void
     */
    public function __construct(ResetPassword $resetPassword)
    {
        $this->resetPassword = $resetPassword;
    }

    /**
     * Handle restore password request.
     *
     * @param RestorePasswordRequest $request
     * @param SmsSender $sender
     * @throws SmsSenderException
     * @return JsonResponse
     */
    public function restorePassword(RestorePasswordRequest $request, SmsSender $sender)
    {
        $resetPasswordRow = $this->resetPassword->create($request->validated()['phone']);

        $sender->sendCode($resetPasswordRow->confirm_code, $request->validated()['phone']);

        return SuccessResponse::show();
    }

    /**
     * Handle confirm restore password request.
     *
     * @param ConfirmRestorePasswordRequest $request
     * @return JsonResponse
     */
    public function confirmRestorePassword(ConfirmRestorePasswordRequest $request)
    {
        $resetPasswordRow = ResetPassword::where('confirm_code', $request->validated()['code'])->first();

        return SuccessResponse::show(['hash' => $resetPasswordRow->hash]);
    }

    /**
     * Handle confirm reset password request.
     *
     * @param ResetPasswordRequest $request
     * @return JsonResponse
     */
    public function resetPassword(ResetPasswordRequest $request)
    {
        $validated = $request->validated();
        ResetPassword::where('hash', $validated['hash'])->first()->make($validated['password']);

        return SuccessResponse::show();
    }
}
