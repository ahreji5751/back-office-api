<?php

namespace App\Http\Controllers\Auth;

use App\Classes\SmsSender;
use App\Http\Requests\Auth\ConfirmPhoneRequest;
use App\Http\Requests\Auth\CreatePasswordRequest;
use App\Http\Requests\Auth\RegisterRequest;
use App\Http\Responses\SuccessResponse;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Exceptions\SmsSenderException;
use Illuminate\Http\JsonResponse;

class RegisterController extends Controller
{
    /**
     * User model instance
     *
     * @var User
     */
    private $user;

    /**
     *  Constructor for LoginController.
     *
     * @param User $user
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Handle register request.
     *
     * @param RegisterRequest $request
     * @param SmsSender $sender
     * @throws SmsSenderException
     * @return JsonResponse
     */
    public function register(RegisterRequest $request, SmsSender $sender)
    {
        $user = $this->user->register($request->validated());

        $sender->sendCode($user->confirm_code, $user->phone);

        return SuccessResponse::show();
    }

    /**
     * Handle confirm phone request.
     *
     * @param ConfirmPhoneRequest $request
     * @return JsonResponse
     */
    public function confirmPhone(ConfirmPhoneRequest $request)
    {
        $this->user->verifyPhone($request->validated()['phone']);

        return SuccessResponse::show();
    }

    /**
     * Handle create password request.
     *
     * @param CreatePasswordRequest $request
     * @return JsonResponse
     */
    public function createPassword(CreatePasswordRequest $request)
    {
        $this->user->createPasswordForPhone($request->validated()['phone'], $request->validated()['password']);

        return SuccessResponse::show();
    }
}
