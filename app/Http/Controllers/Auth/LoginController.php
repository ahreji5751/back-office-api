<?php

namespace App\Http\Controllers\Auth;

use App\Exceptions\Auth\WrongCredentials;
use App\Http\Requests\Auth\LoginRequest;
use App\Http\Controllers\Controller;
use App\Http\Responses\SuccessResponse;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use JWTAuth;

class LoginController extends Controller
{
    /**
     * User model instance
     *
     * @var User
     */
    private $user;

    /**
     *  Constructor for LoginController.
     *
     * @param User $user
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Handle login request.
     *
     * @param LoginRequest $request
     * @throws WrongCredentials
     * @return JsonResponse
     */
    public function login(LoginRequest $request)
    {
        $credentials = array_merge($request->validated(), ['phone_verified' => 1]);

        $token = $this->user->login($credentials);

        return SuccessResponse::show(compact('token'));
    }

    /**
     * Handle me request.
     *
     * @return JsonResponse
     */
    public function me()
    {
        $user = JWTAuth::parseToken()->authenticate();

        return SuccessResponse::show(compact('user'));
    }

    /**
     * Handle logout request.
     *
     * @return JsonResponse
     */
    public function logout()
    {
        JWTAuth::parseToken()->invalidate();

        return SuccessResponse::show();
    }
}
