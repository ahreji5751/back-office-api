<?php

namespace App\Http\Responses;

class SuccessResponse
{
    public static function show($data = [])
    {
        return response()->json($data + [
            'success' => true
        ], 200);
    }
}