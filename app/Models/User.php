<?php

namespace App\Models;

use App\Exceptions\Auth\WrongCredentials;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;
use JWTAuth;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'phone', 'confirm_code'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'email_verified', 'approved', 'confirm_code'
    ];

    public function contractors()
    {
        return $this->belongsToMany(Contractor::class)->withTimestamps();
    }

    public function bankAccounts()
    {
        return $this->morphToMany(BankAccount::class, 'havingBankAccount');
    }

    public function documents()
    {
        return $this->hasMany(Document::class);
    }

    public function products()
    {
        return $this->hasMany(Product::class);
    }

    public function contracts()
    {
        return $this->hasMany(Contract::class);
    }

    public function resetPassword()
    {
        return $this->hasOne(ResetPassword::class);
    }

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'email' => $this->email,
            'phone' => $this->phone,
            'role' => $this->role
        ];
    }

    public function register($user)
    {
        $user = $this->updateOrCreate([
            'phone' => $user['phone'],
        ], [
            'name' => $user['name'],
            'email' => $user['email'],
            'confirm_code' => rand(100000, 1000000)
        ]);

        return $user;
    }

    public function createPasswordForPhone($phone, $password) {
        $user = $this->where('phone', $phone)->first();
        $user->createNewPassword($password);
    }

    public function createNewPassword($password)
    {
        $this->password = bcrypt($password);
        $this->save();
    }

    public function login($credentials)
    {
        if (!$token = JWTAuth::attempt($credentials)) {
            throw new WrongCredentials();
        }

        return $token;
    }

    public function verifyPhone($phone)
    {
        $user = $this->where('phone', $phone)->first();
        $user->phone_verified = 1;
        $user->confirm_code = null;
        $user->save();
    }

}
