<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Contractor extends Model
{
    use SoftDeletes;

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $fillable = [
        'type', 'inn', 'kpp', 'name', 'address', 'phone', 'email'
    ];

    public function users()
    {
        return $this->belongsToMany(User::class)->withTimestamps();
    }

    public function bankAccounts()
    {
        return $this->morphToMany(BankAccount::class, 'havingBankAccount');
    }

    public function documents()
    {
        return $this->hasMany(Document::class);
    }

    public function contracts()
    {
        return $this->hasMany(Contract::class);
    }
}
