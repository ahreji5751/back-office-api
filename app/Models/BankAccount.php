<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BankAccount extends Model
{
    use SoftDeletes;

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'user_type', 'number', 'bank_name', 'bik', 'cor_account'
    ];

    public function havingBankAccount()
    {
        return $this->morphTo();
    }

    public function documents()
    {
        return $this->hasMany(Document::class);
    }
}
