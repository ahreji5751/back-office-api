<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Document extends Model
{
    use SoftDeletes;

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'contractor_id', 'contact_id', 'bank_account_id', 'type', 'number', 'date', 'comment'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function contractor()
    {
        return $this->belongsTo(Contractor::class);
    }

    public function bankAccount()
    {
        return $this->belongsTo(BankAccount::class);
    }

    public function linkedDocuments()
    {
        return $this->belongsToMany(Document::class, 'document_other_document', 'document_id', 'other_document_id');
    }

    public function products()
    {
        return $this->belongsToMany(Product::class)->withTimestamps()->withPivot('count');
    }

    public function contract()
    {
        return $this->belongsTo(Contract::class);
    }

    public function scopeForTheLastYear($query)
    {
        return $query->where('created_at', '>', Carbon::now()->startOfYear());
    }
}
