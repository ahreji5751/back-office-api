<?php

namespace App\Models;

use App\Classes\SmsSender;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;

class ResetPassword extends Model
{
    use SoftDeletes;

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $fillable = [
        'confirm_code', 'hash', 'expired', 'active', 'confirmed'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function create($phone)
    {
        $user = User::where('phone', $phone)->first();

        $resetPasswordRow = $user->resetPassword()->updateOrCreate([
            'user_id' => $user->id
        ],[
            'confirm_code' => rand(100000, 1000000),
            'hash' => Str::random(64),
            'expired' => Carbon::now()->addHour(),
            'confirmed' => false
        ]);

        return $resetPasswordRow;
    }

    public function isExpired()
    {
        return Carbon::parse($this->expired)->lt(Carbon::now());
    }

    public function confirmCodeIsEqualTo($requestCode)
    {
        if ($confirmed = intval($requestCode) === intval($this->confirm_code)) {
            $this->confirmed = true;
            $this->save();
        }

        return $confirmed;
    }

    public function confirmCodePassed($requestCode)
    {
        return !$this->confirmed && $this->confirmCodeIsEqualTo($requestCode) && !$this->isExpired();
    }

    public function make($password)
    {
        $this->user()->first()->createNewPassword($password);
        $this->delete();
    }
}
