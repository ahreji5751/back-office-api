<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Contract;
use Faker\Generator as Faker;

$factory->define(Contract::class, function (Faker $faker) {
    return [
        'contractor_id' => 1,
        'number' => '1A534442',
        'date' => $faker->date()
    ];
});
