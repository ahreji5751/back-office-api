<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Contractor;
use Faker\Generator as Faker;

$factory->define(Contractor::class, function (Faker $faker) {
    return [
        'type' => 'supplier',
        'inn' => $faker->unique()->randomNumber(4),
        'name' => $faker->name,
        'address' => $faker->unique()->address
    ];
});
