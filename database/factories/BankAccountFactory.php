<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\BankAccount;
use Faker\Generator as Faker;

$factory->define(BankAccount::class, function (Faker $faker) {
    return [
        'number' => $faker->unique()->randomNumber(4),
        'bank_name' => 'OP Corporate Bank plc Latvia branch',
        'bik' => 044525545,
        'cor_account' => 30101810300000000545
    ];
});
