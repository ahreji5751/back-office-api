<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Document;
use Faker\Generator as Faker;

$factory->define(Document::class, function (Faker $faker) {
    return [
        'contractor_id' => 1,
        'contract_id' => 1,
        'bank_account_id' => 1,
        'type' => 'invoice',
        'number' => '1A344544',
        'date' => $faker->date(),
        'comment' => $faker->realText(64)
    ];
});
