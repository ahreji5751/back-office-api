<?php

use Illuminate\Database\Seeder;

class SetTestEnvironmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = factory(App\Models\User::class)->create();

        $contractors = factory(App\Models\Contractor::class, 10)->create();

        $contractors->each(function ($contractor) use ($user) {
            $contractor->users()->attach($user->id);
        });

        $bankAccounts = factory(App\Models\BankAccount::class, 5)->create([
            'user_id' => $user->id,
            'user_type' => 'user'
        ]);

        $contracts = factory(App\Models\Contract::class, 10)->create([
            'user_id' => $user->id
        ]);

        factory(App\Models\Document::class, 10)
            ->create([ 'user_id' => $user->id])
            ->each(function ($document) use ($user, $contractors, $bankAccounts, $contracts) {
                $products = factory(App\Models\Product::class, 3)->create(['user_id' => $user->id]);
                $document->products()->attach($products->pluck('id')->toArray(), ['count' => rand(1, 1000)]);

                $contractor = $contractors->random(1)->first();
                $contract = $contracts->random(1)->first();

                $contract->contractor()->associate($contractor);
                $contract->save();

                $document->contractor()->associate($contractor);
                $document->contract()->associate($contract);
                $document->bankAccount()->associate($bankAccounts->random(1)->first());
                $document->save();
            });
    }
}
