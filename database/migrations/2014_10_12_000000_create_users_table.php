<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('phone')->unique();
            $table->boolean('email_verified')->default(false);
            $table->boolean('phone_verified')->default(false);
            $table->string('password')->nullable();
            $table->string('role')->default('client');
            $table->integer('confirm_code')->nullable();
            $table->string('type_of_ownership')->nullable();
            $table->string('full_name')->nullable();
            $table->unsignedBigInteger('inn')->nullable();
            $table->unsignedBigInteger('oktmo')->nullable();
            $table->unsignedBigInteger('ogrn')->nullable();
            $table->string('address')->nullable();
            $table->unsignedInteger('ifns_code')->nullable();
            $table->unsignedInteger('pfr_code')->nullable();
            $table->unsignedBigInteger('fss_code')->nullable();
            $table->string('director_position', 64)->nullable();
            $table->string('director_name', 64)->nullable();
            $table->string('chief_accountant_name', 64)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
