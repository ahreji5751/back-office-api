<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContractorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contractors', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->enum('type', ['supplier', 'buyer']);
            $table->unsignedBigInteger('inn')->unique();
            $table->unsignedInteger('kpp')->nullable();
            $table->string('name')->index();
            $table->string('address', 64)->unique();
            $table->string('phone', 16)->nullable();
            $table->string('email', 128)->nullable();
            $table->float('total_credit_arrears')->nullable();
            $table->float('total_debit_arrears')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contractors');
    }
}
