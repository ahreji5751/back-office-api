<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActDocumentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('act_document', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('act_id')->index();
            $table->unsignedBigInteger('document_id')->index();
            $table->timestamps();

            $table->foreign('act_id')->references('id')->on('acts');
            $table->foreign('document_id')->references('id')->on('documents');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('act_document');
    }
}
