<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => ['throttle:60,1']], function () {

    Route::post('register', ['uses' => 'Auth\RegisterController@register', 'as' => 'register']);
    Route::post('login', ['uses' => 'Auth\LoginController@login', 'as' => 'login']);
    Route::post('confirm-phone', ['uses' => 'Auth\RegisterController@confirmPhone', 'as' => 'confirm-phone']);
    Route::post('create-password', ['uses' => 'Auth\RegisterController@createPassword', 'as' => 'create-password']);
    Route::post('restore-password', ['uses' =>'ForgotPasswordController@restorePassword', 'as' => 'restore-password']);
    Route::post('confirm-restore-password', ['uses' =>'ForgotPasswordController@confirmRestorePassword', 'as' => 'confirm-restore-password']);
    Route::post('reset-password', ['uses' =>'ForgotPasswordController@resetPassword', 'as' => 'reset-password']);

    Route::group(['middleware' => 'auth.jwt'], function () {

        Route::get('logout', ['uses' => 'Auth\LoginController@logout', 'as' => 'logout']);
        Route::get('me', ['uses' => 'Auth\LoginController@me', 'as' => 'me']);

        Route::resource('documents', 'DocumentController');
        Route::resource('contractors', 'ContractorController');

    });

});


