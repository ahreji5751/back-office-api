<?php

return [
    'endpoint' => env('SMS_ENDPOINT'),
    'login' => env('SMS_LOGIN'),
    'password' => env('SMS_PASSWORD'),
    'message' => env('SMS_MESSAGE'),
    'disable_sms' => env('DISABLE_SMS')
];